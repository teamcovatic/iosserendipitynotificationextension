// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SerendipityNotificationServiceExtension",
    platforms: [
        .iOS(.v12)
    ],
    products: [
        .library(
            name: "SerendipityNotificationServiceExtension",
            targets: ["SerendipityNotificationServiceExtension"]),
    ],
    dependencies: [
    ],
    targets: [
        .target(
            name: "SerendipityNotificationServiceExtension",
            dependencies: []),
        .testTarget(
            name: "SerendipityNotificationServiceExtensionTests",
            dependencies: ["SerendipityNotificationServiceExtension"]),
    ]
)
