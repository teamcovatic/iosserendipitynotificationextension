# SerendipityNotificationServiceExtension

Xcode Build Version: 14.0
Swift Tools Version: 5.5

Last updated: 25/01/2023

LatestBuild: 1.0.1


Changes

Support added for notification sounds. Cases handled:
* No sound 
* default sound
* custom sound 
