//
//  File.swift
//  
//
//  Created by Duncan on 24/02/2022.
//

import Foundation

func debugPrint(_ objects: Any...) {
    #if DEBUG
    for item in objects {
        print(item)
    }
    #endif
}
