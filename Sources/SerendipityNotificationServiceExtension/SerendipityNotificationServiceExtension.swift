import Foundation
import UserNotifications

open class SerendipityNotificationServiceExtension: UNNotificationServiceExtension {

    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?

    open override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
       self.contentHandler = contentHandler
       self.bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
                
        guard let bestAttemptContent = bestAttemptContent else {
            debugPrint("bestAttemptContent not valid")
            return
        }
        if request.content.sound != nil {
            if let customSoundPath = bestAttemptContent.userInfo["custom_sound"] as? String {
                let customSound = UNNotificationSound(named: UNNotificationSoundName(rawValue: customSoundPath))
                bestAttemptContent.sound = customSound
            } else {
                let defaultSound  = UNNotificationSound.default
                bestAttemptContent.sound = defaultSound
            }
        }
        guard let mediaAttachmentString = bestAttemptContent.userInfo["imageKey"] as? String else {
            debugPrint("bestAttemptContent key not found")
            contentHandler(bestAttemptContent)
            return
        }
        guard let mediaAttachmentURL = URL(string: mediaAttachmentString) else {
            debugPrint("bestAttemptContent Not a valid URL")
            contentHandler(bestAttemptContent)
            return
        }
        downloadImageFrom(url: mediaAttachmentURL) { attachment in
            guard let mAttachment = attachment else {
                debugPrint("Returned fetchImageFrom attachment closure")
                contentHandler(bestAttemptContent)
                return
            }
            bestAttemptContent.attachments = [mAttachment]
            contentHandler(bestAttemptContent)
        }
    }
    
    open override func serviceExtensionTimeWillExpire() {
        if let contentHandler = contentHandler, let bestAttemptContent = bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }

}

// MARK: - Helper Functions
extension SerendipityNotificationServiceExtension {
    
    private func downloadImageFrom(url: URL, with completionHandler: @escaping (UNNotificationAttachment?) -> Void) {
        let task = URLSession.shared.downloadTask(with: url) { (downloadedUrl, response, error) in
            guard let downloadedUrl = downloadedUrl else {
                completionHandler(nil)
                return
            }
            var urlPath = URL(fileURLWithPath: NSTemporaryDirectory())
            guard let mediaFormat = retrieveMediaComponentsFor(url: url) else {
                completionHandler(nil)
                return
            }
            let uniqueURLEnding = ProcessInfo.processInfo.globallyUniqueString + "." + mediaFormat
            urlPath = urlPath.appendingPathComponent(uniqueURLEnding)
            try? FileManager.default.moveItem(at: downloadedUrl, to: urlPath)
            do {
                var identifier = UUID().uuidString
                identifier.append(".")
                identifier.append(mediaFormat)
                let attachment = try UNNotificationAttachment(identifier: identifier, url: urlPath, options: nil)
                completionHandler(attachment)
            }
            catch {
                completionHandler(nil)
            }
        }
        task.resume()
    }
    
}

private func retrieveMediaComponentsFor(url: URL, extensions: [String] = ["png", "jpg", "jpeg", "gif", "mp4", "mp3"]) -> String? {
    let pathExtention = url.pathExtension
    guard extensions.contains(pathExtention) else {
        return nil
    }
    return pathExtention
}
